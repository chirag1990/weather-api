/**
 * index
 */

"use strict";

export class LocationRoute {


    constructor (locationController) {
        this._locationController = locationController;
    }

    createLocation () {
        return [
            (req, res) => {
                return this._locationController.createLocation(req.params)
                    .then(result => {
                        res.send({success : 1, message : "completed", data : result});
                    })
                    .catch(err => {
                        res.send({success : 0, message : "Error!", data : err.message});
                    });
            }
        ];
    }

    updateLocation () {
        return [
            (req, res) => {
                return this._locationController.updateLocation(req.params)
                    .then(result => {
                        res.send({success : 1, message : "completed", data : result});

                    })
                    .catch(err => {
                        res.send({success : 0, message : "Error!", data : err.message});

                    });

            }
        ];
    }

    getLocation () {
        return [
            (req, res) => {
                return this._locationController.getLocation()
                    .then(result => {
                        res.send({success : 1, message : "completed", data : result});

                    })
                    .catch(err => {
                        res.send({success : 0, message : "Error!", data : err.message});

                    });

            }
        ];
    }

    getLocationById () {
        return [
            (req, res) => {
                return this._locationController.getLocationById(req.params.id)
                    .then(result => {
                        res.send({success : 1, message : "completed", data : result});

                    })
                    .catch(err => {
                        res.send({success : 0, message : "Error!", data : err.message});

                    });

            }
        ];
    }

    deleteLocationById () {
        return [
            (req, res) => {
                return this._locationController.deleteLocationById(req.params.id)
                    .then(result => {
                        res.send({success : 1, message : "completed", data : result});

                    })
                    .catch(err => {
                        res.send({success : 0, message : "Error!", data : err.message});

                    });

            }
        ];
    }

}
