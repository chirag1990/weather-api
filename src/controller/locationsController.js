"use strict";

export class LocationsController {

    constructor(LocationModel, logger) {
        this._locationModel = LocationModel;
        this._logger = logger;
    }

    createLocation(locationData) {
        this._logger.info("Creating new location " + JSON.stringify(locationData));
        let location = new this._locationModel();
        location.name = locationData.name;
        return location.save();
    }

    updateLocation(locationData) {
        this._logger.info("Trying to update location data for : " + locationData.name);
        return this._locationModel.findOne({_id : locationData.id})
            .then (location => {
                if (!location) {
                    this._logger.error("Failed to find location with the id : " + locationData.id);
                    throw new Error("Invalid location id");
                }
                location.name = locationData.name;
                return location.save();
            });
    }

    getLocation() {
        this._logger.info("Trying to get all location data");
        return this._locationModel.find();
    }

    getLocationById(locationId) {
        this._logger.info("Trying to get location by id");
        return this._locationModel.findOne({_id : locationId})
            .then (location => {
                if (!location) {
                    this._logger.error("Failed to find location with the id : " + locationId);
                    throw new Error("Invalid location id");
                }
                return location;
            });
    }

    deleteLocationById(locationId) {
        this._logger.info("Trying to delete location by id");
        return this._locationModel.remove({_id : locationId});
    }
}
