"use strict";

/* Third-party modules */
import _ from "lodash";
import moment from "moment";


export class WeatherController {

	constructor(WeatherModel, logger) {
		this._weatherModel = WeatherModel;
		this._logger = logger;
	}

	createWeatherData(weatherData) {

		return new Promise((fulfill) => {
			this._logger.info("Creating new weather data " + JSON.stringify(weatherData));

			let allPromise = _.map(weatherData, data => {
				let weather = new this._weatherModel();
				weather.locationId = data.locationId;
				weather.precipitation = data.precipitation;
				weather.temp = data.temp;
				weather.hourlyWeather = data.hourlyWeather;
				return weather.save();
			});

			Promise.all(allPromise)
				.then(() => {
					return fulfill("true");
				});
		});
	}


	deleteWeatherDataByLocation(weatherData) {
		this._logger.info("Get weather data using location and date");
		return this._validateFilter(weatherData)
			.then(() => {
				return this._weatherModel.remove({
					"hourlyWeather": {
						$gte: new Date(weatherData.date + "T00:00:00.000Z"),
						$lte: new Date(weatherData.date + "T23:59:59.000Z")
					},
					"locationId": weatherData.locationId
				});
			});
	}

	getWeatherDataByLocation(weatherData) {
		this._logger.info("Get weather data using location and date");
		let _data = {
			"main": {},
			"max": 0,
			"min": 0
		};
		return this._validateFilter(weatherData)
			.then(() => {
				return this._weatherModel.find({
					"hourlyWeather": {
						$gte: new Date(weatherData.date + "T00:00:00.000Z"),
						$lte: new Date(weatherData.date + "T23:59:59.000Z")
					},
					"locationId": weatherData.locationId
				});
			})
			.then((result) => {
				if (result.length === 0) {
					throw new Error("No data was found for a selected date range");
				}
				_data.main = result;
				return this._weatherModel.findOne({
					"hourlyWeather": {
						$gte: new Date(weatherData.date + "T00:00:00.000Z"),
						$lte: new Date(weatherData.date + "T23:59:59.000Z")
					},
					"locationId": weatherData.locationId
				}).sort({temp: -1});
			})
			.then(result => {
				_data.max = result.temp;
				return this._weatherModel.findOne({
					"hourlyWeather": {
						$gte: new Date(weatherData.date + "T00:00:00.000Z"),
						$lte: new Date(weatherData.date + "T23:59:59.000Z")
					},
					"locationId": weatherData.locationId
				}).sort({temp: +1});
			})
			.then(result => {
				_data.min = result.temp;
				return _data;
			});

	}

	_validateFilter(weatherData) {
		return new Promise((fulfill, reject) => {
			if (!weatherData.date || weatherData.date === "") {
				return reject("Date is a required field!");
			}
			if (!weatherData.date || weatherData.date === "") {
				return reject("Date is a required field!");
			}

			return fulfill();
		});
	}

	getWeatherDataByDate(weatherData) {

		let allDates = [];
		this._logger.info("Trying to get weather by id " + weatherData.locationId);
		return this._weatherModel.find({locationId: weatherData.locationId})
			.distinct("hourlyWeather")
			.then(location => {
				if (!location) {
					this._logger.error("Failed to find location with the id : " + weatherData.locationId);
					throw new Error("Invalid location id");
				}
				for (let i = 0; i < location.length; i++) {
					let date = moment(location[i]).format("YYYY-MM-DD");
					let dateString = date.toString();
					if (allDates.indexOf(date) === -1) {
						allDates.push(dateString);
					}
				}
				return allDates;
			});
	}
}
