var mongoose = require("mongoose");

//create an user object
var locationSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    createdAt: String,
    updatedAt: String
});

locationSchema.pre("save", function (next) {
    let now = new Date();
    this.updatedAt = now;
    if (!this.createdAt) {
        this.createdAt = now;
    }
    next();
});


module.exports = mongoose.model("locations", locationSchema);
