## API For Weather APP for Assignment 2

#### CREATE LOCATION

METHOD: POST

URL:

	http://weather.chiragtailor.co.uk/v1/locations
BODY:

	{
  		"name" : "Leicester"
	}

RESPONSE:

	{
  		"success": 1,
		"message": "completed",
		"data": {
			"__v": 0,
			"createdAt": "Thu May 25 2017 22:11:12 GMT+0100 (BST)",
			"updatedAt": "Thu May 25 2017 22:11:12 GMT+0100 (BST)",
			"name": "Nottingham",
			"_id": "592748701f8f5d1995c99e87"
		}
	}


#### GET LOCATION

METHOD: GET

URL:

	http://weather.chiragtailor.co.uk/v1/locations

RESPONSE:

	{
	    "success": 1,
    	"message": "completed",
    	"data": [
        {
          "_id": "592732c5156799f58da7e4fc",
          "createdAt": "Thu May 25 2017 20:38:45 GMT+0100 (BST)",
          "updatedAt": "Thu May 25 2017 20:38:45 GMT+0100 (BST)",
          "name": "Leicester",
          "__v": 0
        },
        {
          "_id": "59273db0cee8e4b4908899d3",
          "createdAt": "Thu May 25 2017 21:25:20 GMT+0100 (BST)",
          "updatedAt": "Thu May 25 2017 21:25:20 GMT+0100 (BST)",
          "name": "Leicester",
          "__v": 0
        },
        {
          "_id": "592748701f8f5d1995c99e87",
          "createdAt": "Thu May 25 2017 22:11:12 GMT+0100 (BST)",
          "updatedAt": "Thu May 25 2017 22:11:12 GMT+0100 (BST)",
          "name": "Nottingham",
          "__v": 0
        }
      ]
  	}

#### DELETE LOCATION

METHOD: DEL

URL:

	http://weather.chiragtailor.co.uk/v1/locations/590749fd99a077a43658008a

BODY: is empty...

RESPONSE:

	{
  		"success": 1,
		"message": "completed",
 		"data": {
    		"ok": 1,
    		"n": 1
  		}
	}


#### CREATING NEW WEATHER DATA

METHOD: POST

URL:

	http://weather.chiragtailor.co.uk/v1/weather-data

BODY :

	  [
        {
          "hourlyWeather" : "Tue May 01 2017 00:00:00 GMT+0000 (UTC)",
          "precipitation" : "Sunny",
          "temp" : 20,
          "locationId" : "59073c03f558e74281e598d9"
        },
        {
          "hourlyWeather" : "Tue May 01 2017 03:00:00 GMT+0000 (UTC)",
          "precipitation" : "rain",
          "temp" : 25,
          "locationId" : "59073c03f558e74281e598d9"
        },
        {
          "hourlyWeather" : "Tue May 01 2017 06:00:00 GMT+0000 (UTC)",
          "precipitation" : "rain",
          "temp" : 25,
          "locationId" : "59073c03f558e74281e598d9"
        },
        {
          "hourlyWeather" : "Tue May 01 2017 09:00:00 GMT+0000 (UTC)",
          "precipitation" : "rain",
          "temp" : 25,
          "locationId" : "59073c03f558e74281e598d9"
        },
        {
          "hourlyWeather" : "Tue May 01 2017 12:00:00 GMT+0000 (UTC)",
          "precipitation" : "rain",
          "temp" : 30,
          "locationId" : "59073c03f558e74281e598d9"
        },
        {
          "hourlyWeather" : "Tue May 01 2017 15:00:00 GMT+0000 (UTC)",
          "precipitation" : "rain",
          "temp" : 32,
          "locationId" : "59073c03f558e74281e598d9"
        },
        {
          "hourlyWeather" : "Tue May 01 2017 18:00:00 GMT+0000 (UTC)",
          "precipitation" : "rain",
          "temp" : 22,
          "locationId" : "59073c03f558e74281e598d9"
        },
        {
          "hourlyWeather" : "Tue May 01 2017 21:00:00 GMT+0000 (UTC)",
          "precipitation" : "rain",
          "temp" : 18,
          "locationId" : "59073c03f558e74281e598d9"
        },
        {
          "hourlyWeather" : "Tue May 01 2017 23:59:99 GMT+0000 (UTC)",
          "precipitation" : "rain",
          "temp" : 18,
          "locationId" : "59073c03f558e74281e598d9"
        }
      ]

RESPONSE:

	{
  		"success": 1,
  		"message": "completed",
  		"data": "true"
	}

#### GET TEMP BY DATE RANGE AND LOCATION

METHOD: POST

URL:

	http://weather.chiragtailor.co.uk/v1/weather-data/location
BODY:

	{
    	"date" : "2017-05-01",
    	"locationId" : "592748701f8f5d1995c99e87"
	}

RESPONSE:

	{
      "success": 1,
      "message": "completed",
      "data": {
        "main": [
          {
            "_id": "59274a4a1f8f5d1995c99e88",
            "createdAt": "Thu May 25 2017 22:19:06 GMT+0100 (BST)",
            "updatedAt": "Thu May 25 2017 22:19:06 GMT+0100 (BST)",
            "hourlyWeather": "2017-05-01T00:00:00.000Z",
            "temp": 20,
            "precipitation": "Sunny",
            "locationId": "592748701f8f5d1995c99e87",
            "__v": 0
          },
          {
            "_id": "59274a4a1f8f5d1995c99e89",
            "createdAt": "Thu May 25 2017 22:19:06 GMT+0100 (BST)",
            "updatedAt": "Thu May 25 2017 22:19:06 GMT+0100 (BST)",
            "hourlyWeather": "2017-05-01T03:00:00.000Z",
            "temp": 25,
            "precipitation": "rain",
            "locationId": "592748701f8f5d1995c99e87",
            "__v": 0
          },
          {
            "_id": "59274a4a1f8f5d1995c99e8a",
            "createdAt": "Thu May 25 2017 22:19:06 GMT+0100 (BST)",
            "updatedAt": "Thu May 25 2017 22:19:06 GMT+0100 (BST)",
            "hourlyWeather": "2017-05-01T06:00:00.000Z",
            "temp": 25,
            "precipitation": "rain",
            "locationId": "592748701f8f5d1995c99e87",
            "__v": 0
          },
          {
            "_id": "59274a4a1f8f5d1995c99e8b",
            "createdAt": "Thu May 25 2017 22:19:06 GMT+0100 (BST)",
            "updatedAt": "Thu May 25 2017 22:19:06 GMT+0100 (BST)",
            "hourlyWeather": "2017-05-01T09:00:00.000Z",
            "temp": 25,
            "precipitation": "rain",
            "locationId": "592748701f8f5d1995c99e87",
            "__v": 0
          },
          {
            "_id": "59274a4a1f8f5d1995c99e8c",
            "createdAt": "Thu May 25 2017 22:19:06 GMT+0100 (BST)",
            "updatedAt": "Thu May 25 2017 22:19:06 GMT+0100 (BST)",
            "hourlyWeather": "2017-05-01T12:00:00.000Z",
            "temp": 30,
            "precipitation": "rain",
            "locationId": "592748701f8f5d1995c99e87",
            "__v": 0
          },
          {
            "_id": "59274a4a1f8f5d1995c99e8d",
            "createdAt": "Thu May 25 2017 22:19:06 GMT+0100 (BST)",
            "updatedAt": "Thu May 25 2017 22:19:06 GMT+0100 (BST)",
            "hourlyWeather": "2017-05-01T15:00:00.000Z",
            "temp": 32,
            "precipitation": "rain",
            "locationId": "592748701f8f5d1995c99e87",
            "__v": 0
          },
          {
            "_id": "59274a4a1f8f5d1995c99e8e",
            "createdAt": "Thu May 25 2017 22:19:06 GMT+0100 (BST)",
            "updatedAt": "Thu May 25 2017 22:19:06 GMT+0100 (BST)",
            "hourlyWeather": "2017-05-01T18:00:00.000Z",
            "temp": 22,
            "precipitation": "rain",
            "locationId": "592748701f8f5d1995c99e87",
            "__v": 0
          },
          {
            "_id": "59274a4a1f8f5d1995c99e8f",
            "createdAt": "Thu May 25 2017 22:19:06 GMT+0100 (BST)",
            "updatedAt": "Thu May 25 2017 22:19:06 GMT+0100 (BST)",
            "hourlyWeather": "2017-05-01T21:00:00.000Z",
            "temp": 18,
            "precipitation": "rain",
            "locationId": "592748701f8f5d1995c99e87",
            "__v": 0
          }
        ],
        "max": 32,
        "min": 18
      }
    }

#### DELETE TEMP BY DATE RANGE AND LOCATION

METHOD: DELETE

URL:

	http://weather.chiragtailor.co.uk/v1/weather-data/location

BODY:

	{
    	"date" : "2017-05-01",
    	"locationId" : "592748701f8f5d1995c99e87"
	}

RESPONSE:

    {
      "success": 1,
      "message": "completed",
      "data": {
        "ok": 1,
        "n": 8
      }
    }

#### GET ALL WEATHER DATA FOR LOCATION (for dates)

METHOD: POST

URL:

	http://weather.chiragtailor.co.uk/v1/weather-data/getByDate

BODY:

	{
    	   "locationId" : "592e700d6216b7d35ad73f41"
	}

RESPONSE:
  {
  "success": 1,
  "message": "completed",
  "data": [
    "01-05-2017",
    "11-05-2017",
    "21-05-2017",
    "31-05-2017",
    "18-05-2017",
    "19-05-2017",
    "20-05-2017",
    "14-05-2017",
    "08-05-2017",
    "09-05-2017"
   ]
 }