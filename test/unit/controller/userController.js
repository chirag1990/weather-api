/**
 * Client Service Test
 */
"use strict";

import {expect, proxyquire, sinon} from "../../helpers/configure";
import {Logger} from "../../../src/lib/logger";
import {UserController} from "../../../src/controller/userController";

let UserModel = require("../../../src/model/userModel");


describe("User Controller Test's", function () {

    describe("Methods", function () {

        describe("#constructor", function () {

            it("should set the user model and logger to the class", function () {

                let logger = new Logger();
                let obj = new UserController(UserModel, logger);
                expect(obj._userModel).to.be.equal(UserModel);
                expect(obj._logger).to.be.equal(logger);
            });
        });

        describe("#createUser", function () {

            it("should create user", function () {

                let returnData = {
                    "name": "Chirag",
                    "email": "chirag.tailor@hotmail.co.uk"
                };

                let userData = {
                    "name": "Chirag",
                    "email": "chirag.tailor@hotmail.co.uk",
                    "password": "test123"
                };

                let logger = new Logger();

                sinon.stub(logger, "info")
                    .returns (true);

                let UserModel = sinon.spy();
                UserModel.prototype.save = sinon.stub().resolves(returnData);

                let userController = new UserController(UserModel, logger);
                return userController.createUser(userData)
                    .then(user => {
                        //console.log(user);
                        expect(UserModel.prototype.save).to.be.callCount(1);
                        expect(user).to.be.equal(returnData);
                        expect(logger.info).callCount(1);
                    });
            });
        });

        describe("#userLogin", function () {

            it("should log user in", function () {

                let returnData = {
                    "name": "Chirag",
                    "email": "chirag.tailor@hotmail.co.uk"
                };

                let userData = {
                    "email": "chirag.tailor@hotmail.co.uk",
                    "password": "1234"
                };

                let logger = new Logger();

                sinon.stub(logger, "info")
                    .returns (true);

                sinon.stub(logger, "error")
                    .returns (true);

                let UserModel =  {
                    findOne: sinon.stub().resolves(returnData)
                };
                let userController = new UserController(UserModel, logger);
                return userController.userLogin(userData)
                    .then(user => {
                        //console.log(user);
                        expect(UserModel.findOne).to.be.callCount(1);
                        expect(user).to.be.equal(returnData);
                        expect(logger.info).callCount(1);
                        expect(logger.error).callCount(0);
                    });
            });

            it("should fail to log user in", function () {

                let userData = {
                    "email": "chirag.tailor@hotmail.co.uk",
                    "password": "1234"
                };

                let logger = new Logger();

                sinon.stub(logger, "info")
                    .returns (true);

                sinon.stub(logger, "error")
                    .returns (true);

                let UserModel =  {
                    findOne: sinon.stub().resolves(null)
                };
                let userController = new UserController(UserModel, logger);
                return userController.userLogin(userData)
                    .then(() => {
                        throw new Error("Test Failed");
                    })
                    .catch(error => {
                        //console.log(user);
                        expect(error).to.be.instanceOf(Error);
                        expect(UserModel.findOne).to.be.callCount(1);
                        expect(logger.info).callCount(1);
                        expect(logger.error).callCount(1);
                    });
            });
        });
    });
});
