var mongoose = require("mongoose");

//create an user object
var weatherSchema = mongoose.Schema({
    locationId: {
        type: String,
        required: true
    },
    precipitation: {
        type: String,
        required: true
    },
    temp: {
        type: Number,
        required: true
    },
    hourlyWeather: {
        type: Date,
        required: true
    },
    createdAt: String,
    updatedAt: String
});

weatherSchema.pre("save", function (next) {
    let now = new Date();
    this.updatedAt = now;
    if (!this.createdAt) {
        this.createdAt = now;
    }
    next();
});

module.exports = mongoose.model("weathers", weatherSchema);
