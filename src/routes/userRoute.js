/**
 * index
 */

"use strict";


/* Node modules */


/* Third-party modules */


/* Files */


export class UserRoute {


    constructor (userController) {
        this._userController = userController;
    }

    createUser () {
        return [
            (req, res) => {
                return this._userController.createUser(req.params)
                    .then(result => {
                        res.send({success : 1, message : "completed", data : result});

                    })
                    .catch(err => {
                        res.send({success : 0, message : "Error!", data : err.message});

                    });

            }
        ];
    }

    userLogin () {
        return [
            (req, res) => {
                return this._userController.userLogin(req.params)
                    .then(result => {
                        res.send({success : 1, message : "completed", data : result});

                    })
                    .catch(err => {
                        res.send({success : 0, message : "Error!", data : err.message});

                    });

            }
        ];
    }
}
