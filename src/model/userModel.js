var mongoose = require("mongoose");

//create an user object
var userSchema = mongoose.Schema({
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    createdAt: String,
    updatedAt: String
});

userSchema.pre("save", function (next) {
    let now = new Date();
    this.updatedAt = now;
    if (!this.createdAt) {
        this.createdAt = now;
    }
    next();
});


module.exports = mongoose.model("users", userSchema);
