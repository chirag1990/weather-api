/**
 * app
 */

"use strict";

import restify from "restify";
import config from "./config.json";

/* Files */
import {Logger} from "./lib/logger";
// import controllers
import {UserController} from "./controller/userController";
import {LocationsController} from "./controller/locationsController";
import {WeatherController} from "./controller/weatherController";

// import routes
import {UserRoute} from "./routes/userRoute";
import {LocationRoute} from "./routes/locationRoute";
import {WeatherRoute} from "./routes/weatherRoute";


//import completed

//Mongoose connection
var mongoose = require("mongoose");
mongoose.connect(config.db.mongodb.string);

// Models
let UserModel = require("./model/userModel");
let LocationModel = require("./model/locationModel");
let WeatherModel = require("./model/weatherModel");


/* client constructor */
let logger = new Logger();  //logging constructor
let userController = new UserController(UserModel, logger);
let userRoute = new UserRoute(userController);

let locationsController = new LocationsController(LocationModel, logger);
let locationRoute = new LocationRoute(locationsController);

let weatherController = new WeatherController(WeatherModel, logger);
let weatherRoute = new WeatherRoute(weatherController, logger);



// Creating a new Server
let server = restify.createServer(); //create a new restify server

server
    .use(restify.gzipResponse())
    .use(restify.bodyParser());

/* Define the routes */
server.post("/v1/users", userRoute.createUser());
server.post("/v1/users/login", userRoute.userLogin());

// routes for locations CRUD
server.post("/v1/locations", locationRoute.createLocation());
server.put("/v1/locations/:id", locationRoute.updateLocation());
server.get("/v1/locations", locationRoute.getLocation());
server.get("/v1/locations/:id", locationRoute.getLocationById());
server.del("/v1/locations/:id", locationRoute.deleteLocationById());


// routes for Weather Data CRUD
server.post("/v1/weather-data", weatherRoute.createWeatherData());
server.post("/v1/weather-data/location", weatherRoute.getWeatherDataByLocation());
server.del("/v1/weather-data/location", weatherRoute.deleteWeatherDataByLocation());
server.post("/v1/weather-data/getByDate", weatherRoute.getWeatherDataByDate());


server.pre(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
    res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, token");
    if (req.headers.token === "62079c94-ef3e-11e5-9ce9-5e5517507c6666fe99d2-ef3e-11e5-9ce9-5e5517507c66") {
        next();
    }
    else {
        res.send({success : 0, message : "Invalid Token", data : {}, retry: 1});
    }
});


/* Start up the server */
server.listen(config.server.port, () => {
    logger.info("System Listen on port " + config.server.port);
});




