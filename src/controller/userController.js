"use strict";

/* Third-party modules */
import sha1 from "sha1";

export class UserController {

    constructor(UserModel, logger) {

        this._userModel = UserModel;
        this._logger = logger;

    }

    createUser(userData) {
        this._logger.info("Creating new user " + JSON.stringify(userData));
        let user = new this._userModel();
        user.name = userData.name;
        user.email = userData.email;
        user.password = sha1(userData.password);
        return user.save();
    }

    userLogin(userData) {
        this._logger.info("Trying to log user in " + userData.email);
        return this._userModel.findOne({email : userData.email, password : sha1(userData.password)})
            .then (user => {
                if (!user) {
                    this._logger.error("Failed to log user in " + userData.email);
                    throw new Error("Invalid user or password");
                }
                return user;
            });
    }
}
