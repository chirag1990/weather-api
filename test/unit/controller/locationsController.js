/**
 * Client Service Test
 */
"use strict";

import {expect, proxyquire, sinon} from "../../helpers/configure";
import {Logger} from "../../../src/lib/logger";
import {LocationsController} from "../../../src/controller/locationsController";

let LocationModel = require("../../../src/model/locationModel");


describe("Location Controller Test's", function () {

    describe("Methods", function () {

        describe("#constructor", function () {

            it("should set the location model and logger to the class", function () {

                let logger = new Logger();
                let obj = new LocationsController(LocationModel, logger);
                expect(obj._locationModel).to.be.equal(LocationModel);
                expect(obj._logger).to.be.equal(logger);
            });
        });


        describe("#createLocation", function () {
            it("should create new location ", function () {

                let locationData = {
                    "name" : "London"
                };

                let logger = new Logger();

                sinon.stub(logger, "info")
                    .returns (true);

                let LocationModel = sinon.spy();
                LocationModel.prototype.save = sinon.stub().resolves(12345);

                let locationsController = new LocationsController(LocationModel, logger);
                return locationsController.createLocation(locationData)
                    .then(location => {
                        //console.log(user);
                        expect(LocationModel.prototype.save).to.be.callCount(1);
                        expect(location).to.be.equal(12345);
                        expect(logger.info).callCount(1);
                    });

            });
        });

        describe("#updateLocation", function () {
            it("should update location ", function () {

                let locationData = {
                    "name" : "London",
                    "id" : "1234"
                };

                let locationDataById = {
                    "name" : "London",
                    "save" : sinon.stub().resolves(12345)
                };


                let logger = new Logger();

                sinon.stub(logger, "info")
                    .returns (true);

                sinon.stub(logger, "error")
                    .returns (true);

                let LocationModel = sinon.spy();
                LocationModel.findOne = sinon.stub().resolves(locationDataById);

                let locationsController = new LocationsController(LocationModel, logger);
                return locationsController.updateLocation(locationData)
                    .then(location => {
                        //console.log(user);
                        expect(locationDataById.save).to.be.callCount(1);
                        expect(LocationModel.findOne).to.be.callCount(1)
                            .calledWith({"_id" : "1234"});
                        expect(location).to.be.equal(12345);
                        expect(logger.info).callCount(1);
                        expect(logger.error).callCount(0);
                    });

            });

            it("should fail to find location by id ", function () {

                let locationData = {
                    "name" : "London",
                    "id" : "1234"
                };

                let logger = new Logger();

                sinon.stub(logger, "info")
                    .returns (true);

                sinon.stub(logger, "error")
                    .returns (true);

                let LocationModel = sinon.spy();
                LocationModel.findOne = sinon.stub().resolves(null);

                let locationsController = new LocationsController(LocationModel, logger);
                return locationsController.updateLocation(locationData)
                    .then(() => {
                        throw new Error("Error");
                    })
                    .catch (err => {
                        expect(LocationModel.findOne).to.be.callCount(1)
                            .calledWith({"_id" : "1234"});
                        expect(logger.info).callCount(1);
                        expect(logger.error).callCount(1);
                        expect(err).to.be.instanceOf(Error);
                    });
            });
        });

        describe("#getLocation", function () {

            it("should get all the locations", function () {

                let locationData = [{
                    "name" : "London"
                }];

                let logger = new Logger();

                sinon.stub(logger, "info")
                    .returns (true);

                let LocationModel = sinon.spy();
                LocationModel.find = sinon.stub().resolves(locationData);

                let locationsController = new LocationsController(LocationModel, logger);
                return locationsController.getLocation(locationData)
                    .then(location => {
                        //console.log(user);
                        expect(LocationModel.find).to.be.callCount(1);
                        expect(location).to.be.equal(locationData);
                        expect(logger.info).callCount(1);
                    });
            });
        });


        describe("#getLocationById", function () {
            it("should get location by id ", function () {

                let locationDataById = {
                    "name" : "London",
                };


                let logger = new Logger();

                sinon.stub(logger, "info")
                    .returns (true);

                sinon.stub(logger, "error")
                    .returns (true);

                let LocationModel = sinon.spy();
                LocationModel.findOne = sinon.stub().resolves(locationDataById);

                let locationsController = new LocationsController(LocationModel, logger);
                return locationsController.getLocationById("1234")
                    .then(location => {
                        //console.log(user);
                        expect(LocationModel.findOne).to.be.callCount(1)
                            .calledWith({"_id" : "1234"});
                        expect(location).to.be.equal(locationDataById);
                        expect(logger.info).callCount(1);
                        expect(logger.error).callCount(0);
                    });

            });
            it("should fail to get location by id ", function () {

                let locationDataById = {
                    "name" : "London",
                };


                let logger = new Logger();

                sinon.stub(logger, "info")
                    .returns (true);

                sinon.stub(logger, "error")
                    .returns (true);

                let LocationModel = sinon.spy();
                LocationModel.findOne = sinon.stub().resolves(null);

                let locationsController = new LocationsController(LocationModel, logger);
                return locationsController.getLocationById("1234")
                    .then(location => {
                        throw new Error("Failed to get location by id");
                    })
                    .catch(error => {
                        //console.log(user);
                        expect(LocationModel.findOne).to.be.callCount(1)
                            .calledWith({"_id" : "1234"});
                        expect(logger.info).callCount(1);
                        expect(logger.error).callCount(1);
                        expect(error).to.be.instanceOf(Error);
                    });
            });
        });


        describe("#deleteLocationById", function () {

            it("should delete location by id", function () {

                let locationData = {
                    "name" : "London"
                };

                let logger = new Logger();

                sinon.stub(logger, "info")
                    .returns (true);

                let LocationModel = sinon.spy();
                LocationModel.remove = sinon.stub().resolves(locationData);

                let locationsController = new LocationsController(LocationModel, logger);
                return locationsController.deleteLocationById("12345")
                    .then(location => {
                        //console.log(user);
                        expect(LocationModel.remove).to.be.callCount(1)
                            .calledWith({"_id" : "12345"});
                        expect(location).to.be.equal(locationData);
                        expect(logger.info).callCount(1);
                    });
            });
        });
    });
});
