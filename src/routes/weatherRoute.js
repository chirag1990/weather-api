/**
 * index
 */

"use strict";

export class WeatherRoute {


    constructor (weatherController, logger) {
        this._weatherController = weatherController;
        this._logger = logger;
    }

    createWeatherData () {
        return [
            (req, res) => {
                return this._weatherController.createWeatherData(req.params)
                    .then(result => {
                        res.send({success : 1, message : "completed", data : result});
                    })
                    .catch(err => {
                        res.send({success : 0, message : "Error!", data : err.message});
                    });
            }
        ];
    }

    getWeatherDataByLocation () {
        return [
            (req, res) => {
                return this._weatherController.getWeatherDataByLocation(req.params)
                    .then(result => {
                        res.send({success : 1, message : "completed", data : result});
                    })
                    .catch(err => {
                        res.send({success : 0, message : "Error!", data : err.message});
                    });
            }
        ];
    }
    deleteWeatherDataByLocation () {
        return [
            (req, res) => {
                return this._weatherController.deleteWeatherDataByLocation(req.params)
                    .then(result => {
                        res.send({success : 1, message : "completed", data : result});
                    })
                    .catch(err => {
                        res.send({success : 0, message : "Error!", data : err.message});
                    });
            }
        ];
    }

    getWeatherDataByDate () {
        return [
            (req, res) => {

                this._logger.info("req.params.id =  " + req.params);
                return this._weatherController.getWeatherDataByDate(req.params)
                    .then(result => {
                        res.send({success : 1, message : "completed", data : result});
                    })
                    .catch(err => {
                        res.send({success : 0, message : "Error!", data : err.message});
                    });
            }
        ];
    }


}
